#!/bin/bash

# constants
LOCAL_COMICS_DIR="$HOME/Comics"
UNCOMPRESS_OUTPUT_DIR="uncompressed"
COMPRESSED_OUTPUT_DIR="compressed"
WORKING_DIR="$LOCAL_COMICS_DIR/$UNCOMPRESS_OUTPUT_DIR" # ~/Comics/output
IMAGES_REGEX=".*\.\(jpg\|png\|jpeg\)"

mkdir "$WORKING_DIR"

extract_image_files(){
	if [[ "$1" == *".cbz" ]]
	then
		# unzip cbz
		# unzip "/home/rurickdev/Comics/Daredevil/Vol 1 y 2 #001-#512/Daredevil - Vol 1 #323.cbz" -d ./output
		unzip "$1" -d "$WORKING_DIR"
	else
		# unrar cbr
		# unrar e "/home/rurickdev/Comics/Daredevil/Vol 1 y 2 #001-#512/Daredevil - Vol 1 #048.cbr" ./output
		unrar e "$1" "$WORKING_DIR"
	fi
}

convert_extracted_images_to_webp(){
	find "$WORKING_DIR" -regex ".*\.\(jpg\|png\|jpeg\)$" -exec cwebp {} -o {}.webp \
}

remove_old_image_files(){
	find "$WORKING_DIR" -regex ".*\.\(jpg\|png\|jpeg\)$" -exec rm {} \
}

clean_converted_files_name(){
	find "$WORKING_DIR" -regex ".*\.\(jpg\|png\|jpeg\).*" |
	while read filename
	do
		mv "$filename" "${filename/(.png|.jpg|.jpeg)/}"
	done
}

compress_to_cbz(){
	# TODO: compress the files from UNCOMPRESS_OUTPUT_DIR and save it in COMPRESSED_OUTPUT_DIR for test, in future remove original and put the new one.
}

empty_output_dir () {
	rm "$WORKING_DIR/*"
}

# Find every cbz and cbr files and iterate over there
find "$LOCAL_COMICS_DIR" -regex '.*\.cb\(z\|r\)$' |
while read comicpath
do 
	# uncompress everi file to get the pictures in ~/Comics/output/
	extract_image_files "$comicpath"

	# if extracted files are webp skip file


	# Covert the images in ~/Comics/output/ to webp
	convert_extracted_images_to_webp

	# Delete all non webp images from ~/Comics/output/
	remove_old_image_files

	# remove the unecesary extension from images name.jpg.webp -> name.webp
	clean_converted_files_name
	# TODO: Compress the files as cbz
	empty_output_dir
done


# find -printf '%p' -exec command \;
